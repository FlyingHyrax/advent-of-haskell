module Main where

import System.Environment

import Dispatch

main = do
    (n:_) <- getArgs
    input <- getContents
    putStrLn $ dispatch (read n) input
