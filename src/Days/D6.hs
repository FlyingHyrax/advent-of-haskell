module Days.D6 ( solver ) where

import SolverTypes
import Text.Parsec.String (Parser)
import Text.Parsec
import Text.Parsec.Combinator
import Text.Parsec.Char (char, digit, spaces)
import Control.Monad (void)

solver :: String -> Solution
solver inp = packIntAns (Just (part1 ins), Just (part2 ins))
    where ins = readInstructions inp

part1 :: [Instruction] -> Int
part1 = length . filter isOn . applyInstructionsToBase brightnessForActionP1

part2 :: [Instruction] -> Int
part2 = sum . map getBrightness . applyInstructionsToBase brightnessForActionP2

applyInstructionsToBase :: BrightnessChanger -> [Instruction] -> [Light]
applyInstructionsToBase bc insts = foldl foldHelp startLights insts
    where foldHelp prevLights inst = map (applyInstruction bc inst) prevLights

readInstructions :: String -> [Instruction]
readInstructions input = case parse input of
                             Right r -> r
                             Left e -> error $ show e
    where parse = simpleParse $ parseInstruction `sepEndBy` endOfLine

data Action = TurnOn | TurnOff | Toggle deriving (Show, Eq)

data Coordinate = Coordinate {
    getx, gety :: Int
} deriving (Eq)

instance Show Coordinate where
    show cr = (show . getx) cr ++ "," ++ (show . gety) cr

instance Bounded Coordinate where
    minBound = Coordinate 0 0
    maxBound = Coordinate 999 999

data GridRect = GridRect {
    gettl, getbr :: Coordinate
} deriving (Eq)

instance Show GridRect where
    show (GridRect s e) = (show s) ++ " .. " ++ (show e)

within :: Coordinate -> GridRect -> Bool
(Coordinate xp yp) `within` (GridRect (Coordinate xtl ytl) (Coordinate xbr ybr)) =
    xinside && yinside
    where xinside = xp >= xtl && xp <= xbr
          yinside = yp >= ytl && yp <= ybr

data Instruction = Instruction {
    action :: Action,
    rect :: GridRect
} deriving (Eq)

instance Show Instruction where
    show inst = (show . action) inst ++ " " ++ (show . rect) inst

data Light = Light {
    getCoord :: Coordinate,
    getBrightness :: Int
} deriving (Show, Eq)

isOn :: Light -> Bool
isOn (Light _ b) = b > 0

startLights :: [Light]
startLights = [Light (Coordinate x y) 0 | x <- [0..999], y <- [0..999]]

type BrightnessChanger = (Action -> Int -> Int)

-- once we use the same data type to represent lights for both problem parts,
-- the only difference is how we apply actions from instructions to change
-- the brightness of a light. That's these two functions

brightnessForActionP1 :: BrightnessChanger
brightnessForActionP1 TurnOn _ = 1
brightnessForActionP1 TurnOff _ = 0
brightnessForActionP1 Toggle b = if b < 1 then 1 else 0

brightnessForActionP2 :: BrightnessChanger
brightnessForActionP2 TurnOn b = b + 1
brightnessForActionP2 TurnOff b = max 0 (b - 1)
brightnessForActionP2 Toggle b = b + 2

-- given arbitrary brightness changing function to translate
-- an action to a new brightness, applies that action to a light
-- to yield a new light
applyAction :: BrightnessChanger -> Action -> Light -> Light
applyAction bc a (Light c b) = Light c (bc a b)

applyInstruction :: BrightnessChanger -> Instruction -> Light -> Light
applyInstruction bc (Instruction a r) l@(Light c _)
    | c `within` r = applyAction bc a l
    | otherwise = l

-----------------------------------------------------------
-- Parsec
-----------------------------

parseAction :: Parser Action
parseAction = do
        s <- eatSpaceAfter $ try (string "turn on") <|> try (string "turn off") <|> string "toggle"
        return (case s of "turn on" -> TurnOn
                          "turn off" -> TurnOff
                          "toggle" -> Toggle)

parseCoordinate :: Parser Coordinate
parseCoordinate = do
    x <- many1 digit
    void $ char ','
    y <- eatSpaceAfter $ many1 digit
    return (Coordinate (read x) (read y))

parseGridRect :: Parser GridRect
parseGridRect = do
    p1 <- parseCoordinate
    void $ eatSpaceAfter $ string "through"
    p2 <- parseCoordinate
    return (GridRect p1 p2)

parseInstruction :: Parser Instruction
parseInstruction = do
    a <- parseAction
    r <- parseGridRect
    return (Instruction a r)

----------

-- to parse the whole file at once:
-- parseInstruction `sepEndBy` newline
-- :t Parser [Instruction]

-- or, do 'lines' on the string and parse each one.

simpleParse :: Parser a -> String -> Either ParseError a
simpleParse p = parse (eatSpaceBefore p) ""

-- using parseInstruction `sepEndBy` newline wasn't working
-- because the "spaces" parser skips ' \n\r\t', not just ' ',
-- so our lexeme-style parser was eating up the trailing newline
-- that we wanted to split statements on
justspace :: Parser ()
justspace = skipMany $ char ' '

eatSpaceBefore :: Parser a -> Parser a
eatSpaceBefore p = do
    void $ justspace
    p

eatSpaceAfter :: Parser a -> Parser a
eatSpaceAfter p = do
    x <- p
    void $ justspace
    return x
