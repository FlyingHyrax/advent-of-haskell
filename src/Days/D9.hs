module Days.D9 ( solver ) where

import SolverTypes
import qualified Data.List as L
import qualified Data.Matrix as M

solver :: String -> Solution
solver input = (Just p1, Just p2)
    where adj = ( createMatrix . readEdges ) input
          paths = L.permutations [1..(M.nrows adj)]
          lengths = L.map (pathLength adj) paths
          p1 = IntAnswer $ L.minimum lengths
          p2 = IntAnswer $ L.maximum lengths

data Edge = Edge
    { city1, city2 :: String
    , dist :: Int
    } deriving (Eq, Show)


-- each line of the input corresponds to an edge
readEdges :: String -> [Edge]
readEdges = map (match . words) . lines
    where match (n1:"to":n2:"=":dstr:_) = Edge n1 n2 (read dstr)

-- unique list of cities in the graph
cities :: [Edge] -> [String]
cities edges = L.nub $ ( map city1 edges ) ++ ( map city2 edges )

-- turn a list of edges into a weighted symmetric adjecency matrix
createMatrix :: [Edge] -> M.Matrix Int
createMatrix edges = L.foldl addEdge ( M.zero n n ) edges
    where indices = L.zip (cities edges) [1..]
          n = L.length indices
          findCoord (Edge c1 c2 _) = do
                                c1n <- L.lookup c1 indices
                                c2n <- L.lookup c2 indices
                                return (c1n, c2n)
          symSet val mat (r, c) =  M.setElem val (r, c) $ M.setElem val (c, r) mat
          addEdge mat e@(Edge _ _ d) = maybe mat (symSet d mat) (findCoord e)

-- length of a path defined by integer nodes
pathLength :: M.Matrix Int -> [Int] -> Int
pathLength _ [] = 0
pathLength _ [x] = 0
pathLength adj ( x:y:ys ) = M.getElem x y adj + pathLength adj (y : ys)
