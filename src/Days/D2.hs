module Days.D2 ( solver ) where

import SolverTypes
import qualified HyraxHelpers.String as HS

data Box = Box Int Int Int

solver :: String -> Solution
solver input = packIntAns (Just (p1 boxes), Just (p2 boxes))
    where boxes = map stringToBox $ lines input
          p1 = solveWith wrappingArea
          p2 = solveWith ribbonLength

solveWith :: (Box -> Int) -> [Box] -> Int
solveWith strat = sum . map strat

-- Becoming an instance of Read seems unecessarily complex for this?
stringToBox :: String -> Box
stringToBox str = case HS.splitOn "x" str of (l:w:h:_) -> Box (read l) (read w) (read h)
                                             _ -> error "Couldn't convert that string into a box"

boxSideAreas :: Box -> [Int]
boxSideAreas (Box l w h) = [l*w, w*h, l*h]

boxSidePerimeters :: Box -> [Int]
boxSidePerimeters (Box l w h) = [p1, p2, p3]
    where p1 = perimHelper l w
          p2 = perimHelper w h
          p3 = perimHelper l h

perimHelper :: Int -> Int -> Int
perimHelper x y = x + x + y + y

boxVolume :: Box -> Int
boxVolume (Box l w h) = l * w * h

wrappingArea :: Box -> Int
wrappingArea b = (sum $ map (*2) sides) + minimum sides
    where sides = boxSideAreas b

ribbonLength :: Box -> Int
ribbonLength b = (minimum $ boxSidePerimeters b) + boxVolume b
