module Days.D1 ( solver ) where

import SolverTypes
import Data.List

solver :: String -> Solution
solver inp = packIntAns (Just p1, Just p2)
    where p1 = part1 inp
          p2 = part2 inp

part1 :: String -> Int
part1 = foldl stepper 0

part2 :: String -> Int
part2 = head . elemIndices (-1) . parensToFloors

-- scan is great.  This is as if we had mapped each paren
-- to the current floor after processing it.
parensToFloors :: [Char] -> [Int]
parensToFloors = scanl stepper 0

stepper :: Int -> Char -> Int
stepper res '(' = res + 1
stepper res ')' = res - 1
