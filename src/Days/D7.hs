module Days.D7 ( solver ) where

import SolverTypes
import qualified Data.Map as Map
import Data.List
import Data.Int
import Data.Bits
import Data.Word

import Text.Parsec
import Text.Parsec.String (Parser)
import Text.Parsec.Char (char, spaces, lower, digit, endOfLine)
import Control.Monad (void)
import Control.Applicative ( (<$>), (<*>) )

-- for testing
import System.IO

solver :: String -> Solution
solver input = packIntAns (Just (part1 circ), Just (part2 circ))
    where circ = (buildCircuit . readInstructions) input

readInstructions :: String -> [Instruction]
readInstructions input = case doparse input of
                            Right r -> r
                            Left e -> error $ show e
    where doparse = parse (parseInstruction `sepEndBy` endOfLine) ""

buildCircuit :: [Instruction] -> Circuit
buildCircuit is = foldl add Map.empty is
    where add circ ( Instruction src id ) = Map.insert id src circ

part1 :: Circuit -> Int
part1 circ = maybe (-1) fromIntegral (Map.lookup "a" results)
    where results = eval circ

part2 :: Circuit -> Int
part2 circ = maybe (-1) fromIntegral (Map.lookup "a" results)
    where a = fromIntegral $ part1 circ
          newcirc = Map.adjust (const $ Raw a) "b" circ
          results = eval newcirc

-- debug func
testing = do
    withFile "inputs/7.txt" ReadMode (\ h -> do
        contents <- hGetContents h
        let circ = (buildCircuit . readInstructions) contents
        putStr (showCirc circ))

showCirc :: Circuit -> String
showCirc = concat . intersperse "\n" . map show . Map.toList

-- well... geez.
-- TY for demonstrating how laziness can make memoization effortless:
-- https://gist.github.com/gglouser/87aec3fc04d3d1ff0b11
 -- ( No credit for me: this is a solution from Reddit adapted to my data types. )
eval :: Map.Map String Source -> Map.Map String Word16
eval circ = results
    where results = Map.map evalSrc circ
          evalSrc :: Source -> Word16
          evalSrc (Raw n) = n
          evalSrc (Wire id) = results Map.! id
          evalSrc (SomeGate g) = evalGate g
              where evalGate :: Gate -> Word16
                    evalGate (NotGate src) = complement $ evalSrc src
                    evalGate (OpGate op lsrc rsrc) = binop lval rval
                        where binop = case op of
                                          AndOp -> (.&.)
                                          OrOp -> (.|.)
                              lval = evalSrc lsrc
                              rval = evalSrc rsrc
                    evalGate (ShiftGate dir src mag) = sh val mag
                        where sh = case dir of
                                       LeftShift -> shiftL
                                       RightShift -> shiftR
                              val = evalSrc src


-- Circuit structure types ---------------------------------

type Signal = Maybe Word16

data Source = Raw Word16 | Wire String | SomeGate Gate
    deriving Eq

type Circuit = Map.Map String Source

data BinaryOp = AndOp | OrOp
    deriving Eq

data ShiftDir = LeftShift | RightShift
    deriving Eq

data Gate = OpGate BinaryOp Source Source
    | ShiftGate ShiftDir Source Int
    | NotGate Source
    deriving Eq


-- custom show instances -----------------------------------

instance Show Source where
    show (Raw n) = show n
    show (Wire s) = s
    show (SomeGate g) = show g

instance Show BinaryOp where
    show AndOp = "AND"
    show OrOp = "OR"

instance Show ShiftDir where
    show LeftShift = "LSH"
    show RightShift = "RSH"

instance Show Gate where
    show (OpGate op l r) =
        "[ " ++ (show l) ++ " " ++ (show op) ++ " " ++ (show r) ++ " ]"
    show (ShiftGate dir inp mag) =
        "[ " ++ (show inp) ++ " " ++ (show dir) ++ " " ++ (show mag) ++ " ]"
    show (NotGate inp) =
        "[ NOT " ++ (show inp) ++ " ]"


-- Parsec --------------------------------------------------

-- intermediate - we'll fold these into a Circuit
data Instruction = Instruction Source String

parseInstruction :: Parser Instruction
parseInstruction = do
    s <- parseSource
    void $ eatTrailingSpace $ string "->"
    id <- parseId
    return (Instruction s id)

parseId :: Parser String
parseId = eatTrailingSpace ( many1 letter )

parseSource :: Parser Source
parseSource = try parseSomeGate <|> parseRaw <|> parseWire
-- try is required because both a gate and just some value start with digits

parseWire :: Parser Source
parseWire = Wire <$> parseId

parseRaw :: Parser Source
parseRaw = Raw <$> read <$> eatTrailingSpace ( many1 digit )

parseSomeGate :: Parser Source
parseSomeGate = SomeGate <$> parseGate

parseWireOrRaw:: Parser Source
parseWireOrRaw = parseWire <|> parseRaw

-- ...but what about the gates?
-- NOT gates are easy... the rest are all munged.
parseGate :: Parser Gate
parseGate = parseNotGate <|> parseOtherGate

-- Not gates at least are very straightforward...
parseNotGate :: Parser Gate
parseNotGate = do
    void $ eatTrailingSpace $ string "NOT"
    s <- parseWireOrRaw
    return ( NotGate s )

parseOtherGate :: Parser Gate
parseOtherGate = do
    leftSource <- parseWireOrRaw
    gateMaker <- parseRestOfGate
    return (gateMaker leftSource)

-- doesn't return a struct, but a function that will produce a struct
-- once given the source to fill in the left side of the gate, the '_'
-- in '_ AND a' or '_ LSHIFT 2'
parseRestOfGate :: Parser (Source -> Gate)
parseRestOfGate = parseRestOfAndOr <|> parseRestOfShift

parseRestOfAndOr :: Parser (Source -> Gate)
parseRestOfAndOr = do
    opstr <- eatTrailingSpace $ (string "AND" <|> string "OR")
    let op = case opstr of "AND" -> AndOp; "OR" -> OrOp
    rhs <- parseWireOrRaw
    return (\ lhs -> OpGate op lhs rhs)

parseRestOfShift :: Parser (Source -> Gate)
parseRestOfShift = do
    opstr <- eatTrailingSpace $ (string "LSHIFT" <|> string "RSHIFT")
    let dir = case head opstr of 'L' -> LeftShift; 'R' -> RightShift
    mag <- read <$> eatTrailingSpace ( many1 digit )
    return (\ lhs -> ShiftGate dir lhs mag)

eatTrailingSpace :: Parser a -> Parser a
eatTrailingSpace p = do
    r <- p
    skipMany $ char ' '
    return r

