module Days.D3 ( solver ) where

import SolverTypes
import qualified Data.Set as Set
import qualified Data.List as List

solver i = packIntAns (Just (part1 i), Just (part2 i))

-- directions
data Dir = North | South | East | West deriving (Eq, Show, Read)

-- map characters to directions
charToDir :: Char -> Dir
charToDir c = case c of '^' -> North
                        'v' -> South
                        '>' -> East
                        '<' -> West

-- positions
data Coord = Coord Int Int deriving (Eq, Show)
origin = Coord 0 0

-- move a coordinate given a direction
moveDir :: Dir -> Coord -> Coord
moveDir North = goNorth
moveDir South = goSouth
moveDir East = goEast
moveDir West = goWest

goNorth :: Coord -> Coord
goNorth (Coord x y) = Coord x (y + 1)

goSouth :: Coord -> Coord
goSouth (Coord x y) = Coord x (y - 1)

goWest :: Coord -> Coord
goWest (Coord x y) = Coord (x - 1) y

goEast :: Coord -> Coord
goEast (Coord x y) = Coord (x + 1) y

-- For Set Coord
instance Ord Coord where
    compare c1@(Coord x1 y1) c2@(Coord x2 y2)
        | c1 == c2 = EQ
        | x1 < x2 = LT
        | x1 == x2 && y1 < y2 = LT
        | otherwise = GT

-- we'll track santa using his current location
-- and a set of places he's been
data SantaTrack = SantaTrack {
    current :: Coord,
    visited :: Set.Set Coord
    } deriving (Show, Eq)
emptyTrack = SantaTrack origin (Set.singleton origin)

-- assuming that we start at (0,0), given a list of directions
-- give back a set of all the places we've visited
trackVisited :: [Dir] -> Set.Set Coord
trackVisited moves = visited $ foldl foldHelper emptyTrack moves

foldHelper :: SantaTrack -> Dir -> SantaTrack
foldHelper (SantaTrack current visited) nextDir = SantaTrack nextPos (Set.insert nextPos visited)
    where nextPos = moveDir nextDir current

-- cool
part1 :: String -> Int
part1 input = length $ trackVisited moves
    where moves = map charToDir input

-- for part 2, we need to divide the directions into 2 lists,
-- taking alternate indices. Then fold up two tracks and union
-- the visited sets.
part2 :: String -> Int
part2 input = length (santaVisited `Set.union` robotVisited)
    where (santaMoves, robotMoves) = divideMoves (map charToDir input)
          santaVisited = trackVisited santaMoves
          robotVisited = trackVisited robotMoves

-- somehow I still feel like Im repeating myself too much
divideMoves :: [Dir] -> ([Dir], [Dir])
divideMoves allMoves = (map snd santaIndexed, map snd robotIndexed)
    where allIndexed = zip [1..] allMoves
          (santaIndexed, robotIndexed) = List.partition (odd . fst) allIndexed
