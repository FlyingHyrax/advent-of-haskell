module Days.D8 ( solver ) where

import SolverTypes
import Data.Char
import System.IO

{- Key insight...
You don't need to actually escape/unescape the string to determine
what it's length *would* be if it were escaped/unescaped.
-}

solver :: String -> Solution
solver input = packIntAns (Just (part1 theLines), Just (part2 theLines))
    where theLines = lines input

part1 :: [String] -> Int
part1 inputs = (sum raw) - (sum unescaped)
    where raw = map length inputs
          unescaped = map countMemChars inputs

part2 :: [String] -> Int
part2 inputs = (sum $ map newlen inputs) - (sum $ map length inputs)
    where newlen = foldl lenhelp 2
          lenhelp n c = n + if c `elem` "\'\"\\" then 2 else 1


{- WHY THIS DOESN'T WORK
Haskell will read more than 2 hexadecimal digits after \x, so
"\x32f" becomes "\815" (1 char) instead of "\50f" (2 chars)


part1 :: [String] -> Int
part1 inputs = (sum raw) - (sum unescaped)
    where raw = map length inputs
          unescaped = map (length . reread) inputs
          reread :: String -> String
          reread = read
-}


-- defined a state machine for counting characters in part 1
data Q
    = ExpQuote
    | ExpAny
    | ExpEsc
    | ExpFstHex
    | ExpSndHex
    | Finish
    | Error
    deriving (Eq, Show)

countMemChars :: String -> Int
countMemChars = snd . foldl delta (ExpQuote, 0)

delta :: (Q, Int) -> Char -> (Q, Int)
delta e@(Error, n) _ = e
delta (ExpQuote, n) c = deltaExpQuote n c
delta (ExpAny, n) c = deltaExpAny n c
delta (ExpEsc, n) c = deltaExpEsc n c
delta (ExpFstHex, n) c = deltaExpFstHex n c
delta (ExpSndHex, n) c = deltaExpSndHex n c
delta (Finish, n) c = (Error, n)

deltaExpQuote :: Int -> Char -> (Q, Int)
deltaExpQuote n '\"' = (ExpAny, n)
deltaExpQuote n _ = (Error, n)

deltaExpAny :: Int -> Char -> (Q, Int)
deltaExpAny n '\"' = (Finish, n)
deltaExpAny n '\\' = (ExpEsc, n)
deltaExpAny n _ = (ExpAny, n + 1)

deltaExpEsc :: Int -> Char -> (Q, Int)
deltaExpEsc n 'x' = (ExpFstHex, n)
deltaExpEsc n c
    | c `elem` "\\\'\"" = (ExpAny, n + 1)
    | otherwise = (Error, n)

deltaExpFstHex :: Int -> Char -> (Q, Int)
deltaExpFstHex n c
    | isHexDigit c = (ExpSndHex, n)
    | otherwise = (Error, n)

deltaExpSndHex :: Int -> Char -> (Q, Int)
deltaExpSndHex n c
    | isHexDigit c = (ExpAny, n + 1)
    | otherwise = (Error, n)
