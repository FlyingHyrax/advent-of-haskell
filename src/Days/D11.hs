module Days.D11 ( solver ) where

import SolverTypes
import qualified Data.List as L
import HyraxHelpers.String (slowStrip)
import HyraxHelpers.Predicate

-- oh dang, everything just BROKE because I assumed that
-- ALL ANSWERS WOULD BE NUMERIC and that is NOT THE CASE.
solver :: String -> Solution
solver input = ((Just . StringAnswer) p1s, (Just . StringAnswer) p2s)
    where trimmed = slowStrip input
          (p1s:rs) = filter (hasStraight .& noBadChars .& hasTwoPairs) $ pwseq trimmed
          p2s = head rs

-- let's write predicates!

{- 1) Passwords must include one increasing straight of at
 - least three letters, like abc, bcd, cde, and so on, up to xyz.
 - They cannot skip letters; abd doesn't count.
 -}
hasStraight :: String -> Bool
hasStraight (x:y:z:zs) = (y == succ x && z == succ y) || hasStraight (y:z:zs)
hasStraight _ = False

{- 2) Passwords may not contain the letters i, o, or l,
 - as these letters can be mistaken for other characters and are therefore confusing.
 -}
noBadChars :: String -> Bool
noBadChars = L.null . L.intersect "iol"

{- 3) Passwords must contain at least two different,
 - non-overlapping pairs of letters, like aa, bb, or zz.
 -}
hasTwoPairs :: String -> Bool
hasTwoPairs str = numPairs >= 2
    where numPairs = L.length $ L.filter ((==2) . L.length) $ L.group str

-- increment strings per santa's rules, ignoring whether or not the result
-- is a valid password (we filter later!)
incrementPw :: String -> String
incrementPw = L.reverse . inchelp . L.reverse
    where inchelp [] = []
          inchelp (x:xs) = (nextChar x) : if x == 'z'
                                          then inchelp xs
                                          else xs
          nextChar 'z' = 'a'
          nextChar c = succ c


-- infinite sequence of incremented passwords
pwseq :: String -> [String]
pwseq = L.unfoldr (\ s -> Just (s, incrementPw s))

validpwseq :: String -> [String]
validpwseq = filter (hasStraight .& noBadChars .& hasTwoPairs) . pwseq
