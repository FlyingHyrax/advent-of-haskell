module Days.D5 ( solver ) where

import SolverTypes
import qualified Data.Set as Set
import qualified Data.List as List
import Text.Regex.Base
import Text.Regex.PCRE.String
import HyraxHelpers.Predicate

-- counts the number of times certain individual characters appear in a string
countChars :: Set.Set Char -> String -> Int
countChars targets "" = 0
countChars targets (x:xs) = it + countVowels xs
    where it = if Set.member x targets then 1 else 0

-- then specialized to vowels
vowels :: Set.Set Char
vowels = Set.fromList "aeiou"

countVowels :: String -> Int
countVowels = countChars vowels

-- then specialized to three vowels
hasThreeVowels :: String -> Bool
hasThreeVowels str = countVowels str >= 3

-- name says it all
doubleLetterHelper :: Maybe Char -> String -> Bool
doubleLetterHelper _ "" = False
doubleLetterHelper Nothing (x:xs) = doubleLetterHelper (Just x) xs
doubleLetterHelper (Just c) (x:xs) = if c == x
                                     then True
                                     else doubleLetterHelper (Just x) xs

-- check if a string contains the same letter twice in a row
hasDoubleLetter :: String -> Bool
hasDoubleLetter = doubleLetterHelper Nothing

-- check if a string contains any substring from a set of strings
hasAnyString :: Set.Set String -> String -> Bool
hasAnyString bads str = any ((flip List.isInfixOf) str) bads

badStrings :: Set.Set String
badStrings = Set.fromList ["ab", "cd", "pq", "xy"]

hasBadString :: String -> Bool
hasBadString = hasAnyString badStrings

part1NiceString :: String -> Bool
part1NiceString = (hasThreeVowels .& hasDoubleLetter .& (not . hasBadString))

-- For part 2, you CLEARLY want to use PCRE RegExp (for backreferences!)
-- 1. (\w{2})\w*?\1
-- 2. (\w)\w\1
-- ...done. :P

rule1re :: Regex
rule1re = makeRegex "(\\w{2})\\w*?\\1"

rule2re :: Regex
rule2re = makeRegex "(\\w)\\w\\1"

part2NiceString :: String -> Bool
part2NiceString = (matchTest rule1re) .& (matchTest rule2re)


countNiceStrings :: Pred String -> [String] -> Int
countNiceStrings test = length . (filter test)

makePartSolver :: Pred String -> String -> Int
makePartSolver test = (countNiceStrings test) . lines

part1 :: String -> Int
part1 = makePartSolver part1NiceString

part2 :: String -> Int
part2 = makePartSolver part2NiceString

solver :: String -> Solution
solver inp = packIntAns (Just (part1 inp), Just (part2 inp))
