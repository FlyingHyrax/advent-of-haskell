module Days.D4 ( solver ) where

-- FIXME: this is really slow; can we make it faster?

import SolverTypes
import Data.Digest.Pure.MD5
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.List as List

solver :: String -> Solution
solver inp = packIntAns (p1, p2)
    where p1 = (search p1target) inp
          p2 = (search p2target) inp

search :: String -> String -> Maybe Int
search fp inp = fmap snd result
    where it guess = hasFingerprint fp (fst guess)
          result = List.find it (trySalts inp)

p1target :: String
p1target = "00000"

p2target :: String
p2target = "000000"

hasFingerprint :: String -> String -> Bool
hasFingerprint = List.isPrefixOf

trySalts :: String -> [(String, Int)]
trySalts base = map (salted base) [1..]

salted :: String -> Int -> (String, Int)
salted base num = (show (md5 bstr), num)
    where bstr = B.pack (base ++ (show num))
