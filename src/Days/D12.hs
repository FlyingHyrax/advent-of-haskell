module Days.D12 ( solver ) where

import SolverTypes

import Text.JSON
import qualified Data.List as L

solver :: String -> Solution
solver input = (Just p1, Just p2)
    where p1 = sumWith sumJson input
          p2 = sumWith sumJson2 input

sumWith :: (RealFrac a) => (JSValue -> a) -> String -> Answer
sumWith s i = case decodeStrict i of
                  Ok js -> IntAnswer $ round $ s js
                  Error e -> StringAnswer e

sumJson :: (Fractional a) => JSValue -> a
sumJson (JSRational asFlt val) = fromRational val -- ???
sumJson (JSArray vals) = sum $ map sumJson vals
sumJson (JSObject vals) = sum $ map (sumJson . snd) $ fromJSObject vals
sumJson _ = 0 -- Null, Bool, String

sumJson2 :: (Fractional a) => JSValue -> a
sumJson2 (JSRational _ val) = fromRational val
sumJson2 (JSArray vals) = sum $ map sumJson2 vals
sumJson2 (JSObject vals) = if hasRed assoc
                           then 0
                           else sum $ map (sumJson2 . snd) $ fromJSObject vals
    where assoc = fromJSObject vals
sumJson2 _ = 0

isRed :: (String, JSValue) -> Bool
isRed (_, JSString jstr) = (fromJSString jstr) == "red"
isRed _ = False

hasRed :: [(String, JSValue)] -> Bool
hasRed assoc = case L.find isRed assoc of
                   Just _ -> True
                   Nothing -> False

