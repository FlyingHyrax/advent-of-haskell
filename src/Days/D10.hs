module Days.D10 ( solver ) where

import SolverTypes
import qualified Data.List as L
import qualified HyraxHelpers.String as HS

solver :: String -> Solution
solver input = packIntAns (Just (length p1), Just (length p2))
    where trimmed = HS.rstrip input
          p1 = church 40 lookAndSay trimmed
          p2 = church 10 lookAndSay p1


lookAndSay :: String -> String
lookAndSay = L.concat . L.map sayGroup . L.group
    where sayGroup s@(x:_) = ( show $ length s ) ++ [ x ]

church :: Int -> (a -> a) -> a -> a
church n f x = fs x
    where fs = foldl (.) id ( replicate n f )

