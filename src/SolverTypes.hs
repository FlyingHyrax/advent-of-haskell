module SolverTypes
( Answer(..)
, Solution
, Solver
, packIntAns
) where

-- Answers can be either Ints or Strings (argh)
data Answer = IntAnswer Int | StringAnswer String

instance Show Answer where
    show (IntAnswer n)    = show n
    show (StringAnswer s) = show s

-- The solution for a day will have the answer for one or both parts
type Solution = (Maybe Answer, Maybe Answer)

-- A solver is a function from a string input to a solution
type Solver = (String -> Solution)

-- for converting output of old solvers to new format
packIntAns :: (Maybe Int, Maybe Int) -> Solution
packIntAns (m1, m2) = (IntAnswer <$> m1, IntAnswer <$> m2)

