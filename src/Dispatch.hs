module Dispatch ( dispatch ) where

-- I wonder if there is a way of automatically enumerating
-- packages in a directory... probably not.
-- but there's still an unfortunate amount of repetition here
import Data.List as List

import SolverTypes
import qualified Days.D1 as D1
import qualified Days.D2 as D2
import qualified Days.D3 as D3
import qualified Days.D4 as D4
import qualified Days.D5 as D5
import qualified Days.D6 as D6
import qualified Days.D7 as D7
import qualified Days.D8 as D8
import qualified Days.D9 as D9
import qualified Days.D10 as D10
import qualified Days.D11 as D11
import qualified Days.D12 as D12

solvers :: [(Int, Solver)]
solvers = List.zip [1..] [ D1.solver
                         , D2.solver
                         , D3.solver
                         , D4.solver
                         , D5.solver
                         , D6.solver
                         , D7.solver
                         , D8.solver
                         , D9.solver
                         , D10.solver
                         , D11.solver
                         , D12.solver
                         ]

dispatch :: Int -> String -> String
dispatch n input = prefix ++ (maybe "unimplemented" (runSolver input) (List.lookup n solvers))
    where prefix = (show n) ++ ": "
          runSolver i s = strForSol $ s i
              where strForSol (m1, m2) = (strForAns m1) ++ ", " ++ (strForAns m2)
                        where strForAns = maybe "unimplemented" show

