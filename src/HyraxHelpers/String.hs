module HyraxHelpers.String
( splitOn
, splitWhen
, slowStrip
, lstrip
, rstrip
)
where

import Data.Char (isSpace)

-- TY: http://stackoverflow.com/a/4981265
splitOn :: String -> String -> [String]
splitOn delims = splitWhen (\ c -> c `elem` delims)

splitWhen :: (Char -> Bool) -> String -> [String]
splitWhen p s = case dropWhile p s of
                    "" -> []
                    s' -> w : splitWhen p s''
                        where (w, s'') = break p s'

slowStrip :: String -> String
slowStrip = lstrip . rstrip

lstrip :: String -> String
lstrip = dropWhile isSpace

rstrip :: String -> String
rstrip = reverse . lstrip . reverse
