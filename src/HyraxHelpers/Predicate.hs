module HyraxHelpers.Predicate
    ( Pred
    , allp
    , anyp
    , (.|)
    , (.&)
    ) where

type Pred a = (a -> Bool)

-- check a single value against a list of predicates
-- (in contrast to all, which checks a list of values against a single predicate)
allp ::  [Pred a] -> a -> Bool
allp ps v = and $ map ($ v) ps

-- like above, but for or/any
anyp ::  [Pred a] -> a -> Bool
anyp ps v = or $ map ($ v) ps

-- set precedences so these combinators are
-- below regular &&/||, above $, and & > | like
-- with the regular boolean ops.
-- Not sure if being lower than regaulr && is good or bad.
(.|) :: Pred a -> Pred a -> Pred a
infixr 1 .|
p1 .| p2 = \x -> (p1 x) || (p2 x)

-- higher than $
(.&) :: Pred a -> Pred a -> Pred a
infixr 2 .&
p1 .& p2 = \x -> (p1 x) && (p2 x)
